//
//  ViewController.swift
//  KDCircularProgressExample
//
//  Created by Kaan Dedeoglu on 2/12/15.
//  Copyright (c) 2015 Kaan Dedeoglu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var button:UIButton!
    @IBOutlet var label:UILabel!

    var progress: KDCircularProgress!
    var length: Int = 0
    var initialLength: Int = 0
    var seconds: NSTimeInterval = 0
    var time:Int = 0
    var initialTime:Int = 0
    var previousPoint = CGPointZero
    var currentPoint = CGPointZero
    var timer:NSTimer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        progress = KDCircularProgress(frame: CGRect(x: 0, y: 0, width: 300, height: 300))
        progress.startAngle = -90
        progress.progressThickness = 0.4
        progress.trackThickness = 0.4
        progress.trackColor = UIColor.grayColor()
        progress.clockwise = true
        progress.center = view.center
        progress.gradientRotateSpeed = 2
        progress.roundedCorners = true
        progress.glowMode = .Forward
        progress.glowAmount = 0.0
        progress.setColors(UIColor.redColor())
        view.addSubview(progress)
        
        let vertical = UISwipeGestureRecognizer(target: self, action: "changeAngle")
        vertical.direction = .Up | .Down
        view.addGestureRecognizer(vertical)
        
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        super.touchesBegan(touches, withEvent: event)
        let touch = touches.first as! UITouch
        let point = touch.locationInView(view)
        previousPoint = point
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        super.touchesMoved(touches, withEvent: event)
        let touch = touches.first as! UITouch
        let point = touch.locationInView(view)
        currentPoint = point
        changeAngle()
    }
    
    func changeAngle(){
        let distance = distanceBetweenPoints(previousPoint, second: currentPoint)
        
        let screenSize = UIScreen.mainScreen().bounds.height
        
        let lengthInCircle = (distance/screenSize)*360
        
        time = Int(round((distance/screenSize) * 60))
        
        seconds = NSTimeInterval(round((distance/screenSize) * 60))
        
        label.text = "\(time) s"
        
        initialTime = time
        
        progress.angle = Int(round(lengthInCircle))
        
        initialLength = Int(round(lengthInCircle))
        
    }
    
    func distanceBetweenPoints(first:CGPoint, second:CGPoint) -> CGFloat {
        let deltaX = second.x - first.x
        let deltaY = second.y - first.y
        return sqrt(deltaX * deltaX + deltaY * deltaY)
    }
    
    @IBAction func animateButtonTapped(sender: UIButton) {

        if button.titleLabel?.text == "Start" &&  time > 0{
            
            timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "timerUpdate", userInfo: nil, repeats: true)
            timer.fire()
            
            button.setTitle("Stop", forState: .Normal)
            
            progress.animateToAngle(0, duration: seconds) { completed in
                
                if completed {
                    self.timer.invalidate()
                    self.button.setTitle("Finish", forState: .Normal)
                    self.button.enabled = false
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(2 * NSEC_PER_SEC)), dispatch_get_main_queue(),
                        { self.button.setTitle("Start", forState: .Normal)
                            self.button.enabled = true})
                }
            }
            
        }else if button.titleLabel?.text == "Stop"{
            timer.invalidate()
            
            button.setTitle("Start", forState: .Normal)
            button.enabled = true
            
            progress.animateToAngle(self.initialLength, duration: 0.5, completion: nil)
            
            time = initialTime
            
            label.text = "\(self.initialTime) s"
    
        }

        
    }
    
    func timerUpdate(){
        label.text = "\(time--) s"
    }
}

